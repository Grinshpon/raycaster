2.5d raycasting demo, inspired by Wolfenstein 3D, written in C using [Gunslinger](https://github.com/MrFrenik/gunslinger). To compile, fork Gunslinger and place it in an "include" folder.
