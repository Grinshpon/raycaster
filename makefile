#SHELL := pwsh -NoProfile

NAME=test

ifeq ($(OS), Windows_NT)
  EXE=$(NAME).exe
  REMOVE=powershell rm
else
  EXE=$(NAME)
  REMOVE=rm
endif

#ASSET_DIR=assets
#TEXTURES=$(wildcard $(ASSET_DIR)/*.png)
SRC_DIR=src
INC_DIR=include
ASSET_DIR=assets
MAIN=$(SRC_DIR)/main.c
SRC_FILES=$(wildcard $(SRC_DIR)/*.c)
H_FILES=$(wildcard $(SRC_DIR)/*.h)

GS_H=$(INC_DIR)/gunslinger.h
#GS_C=$(INC_DIR)/gunslinger.c

CC=clang
#CC=cl

FLAGS= -std=c11 -I$(INC_DIR) -I$(ASSET_DIR) -pthread
#-O3

#debug flags
DFLAGS= -g -fno-inline -fno-omit-frame-pointer

LIBS= -lopengl32 -lkernel32 -luser32 -lshell32 -lgdi32 -lAdvapi32 -lwinmm
#extra flags
#EFLAGS= -pthread

$(MAIN): $(SRC_FILES)

$(EXE): $(MAIN) $(H_FILES)
	$(CC) $(STD) $(FLAGS) $(LIBS) $(SRC_FILES) -o $@

build: $(EXE)

debug: FLAGS += $(DFLAGS)
debug: build

run: build
	./$(EXE)

.PHONY: clean
clean:
	$(REMOVE) $(EXE)
