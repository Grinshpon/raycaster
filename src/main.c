//todo:
//raycaster render steps:
// create a dynamic texture with same dimensions as game-window (look at sample_texture ex and gs_graphics_texture_request_update)
// create a h/z buffer
// "draw" lines and sprites to the texture, then draw texture using gsi
// finally, draw hud/ui

#define GS_IMPL
#include <gs/gs.h>

#define GS_IMMEDIATE_DRAW_IMPL
#include <gs/util/gs_idraw.h>

#define GS_ASSET_IMPL
#include <gs/util/gs_asset.h>

#include <math.h>

#define is_even(x) ((x & 1) == 0)
#define is_odd(x) ((x & 1) == 1)

gs_command_buffer_t gcb  = {0};
gs_asset_manager_t gsa = {0};
gs_immediate_draw_t gsi = {0};

gs_asset_t wall_tex_hndl = {0};
gs_asset_t floor_tex_hndl = {0};
gs_asset_t ceil_tex_hndl = {0};
gs_asset_t barrel_tex_hndl = {0};
gs_asset_t fnt_handle = {0};

void vp_draw_pixel(int x, int y, gs_color_t c);
void vp_draw_strip(int x, int h, gs_color_t c);
void vp_draw_tex(int vx, int vy, int tx0, int ty0, int tx1, int ty1, gs_graphics_texture_desc_t *td);
void vp_draw_strip_tex(int x, int h, int s, gs_graphics_texture_desc_t *td, gs_color_t color_mod);
gs_color_t texture_get_point(gs_graphics_texture_desc_t *td, int x, int y);

// delta time and elapsed time
float dt;
float et;
float fps_timer = 0.f;
float fps;

#define VP_WIDTH 320
#define VP_HEIGHT 200
gs_color_t viewport[VP_WIDTH*VP_HEIGHT] = {0};
gs_asset_texture_t vp_tex = {0};
float zbuf[VP_WIDTH] = {0};
//stencil buffer. true cell means can be written, false means can't be written
bool stencil[VP_WIDTH*VP_HEIGHT] = {true};
bool stencil_enabled = true;

void zbuf_clear() {
  memset(zbuf, 0, sizeof(float)*VP_WIDTH);
}

//level
typedef struct {
  int wall;
  int floor;
} tile_t;

#define WALL (tile_t){.wall=1}
#define HALL (tile_t){.wall=0, .floor=1}

const int map_width = 10;
const int map_height = 10;
tile_t level[map_width*map_height] = {
  WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL,
  WALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, WALL, WALL,
  WALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, WALL,
  WALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, WALL,
  WALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, WALL,
  WALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, HALL, WALL,
  WALL, HALL, HALL, HALL, WALL, HALL, HALL, HALL, HALL, WALL,
  WALL, HALL, HALL, HALL, WALL, HALL, HALL, HALL, HALL, WALL,
  WALL, HALL, HALL, HALL, WALL, HALL, HALL, HALL, HALL, WALL,
  WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL,
};

//player camera
const float fov = GS_PI/3.f;
const float ray_angle = fov / (float)VP_WIDTH;
float plane_mag;
//how far a wall gets before not being rendered
int dof = 20;

// dir is in radians
typedef struct {
  gs_vec2 pos;
  float dir;
} actor_t;

typedef struct {
  
} actor_render_t;

struct {
  actor_t tf;
  actor_render_t r;
} barrel;

actor_t actor_new(float x, float y, float dir) {
  return (actor_t) {
    .pos = gs_v2(x,y),
    .dir = dir
  };
}

actor_t player;

void stencil_clear() {
  memset(stencil, true, sizeof(bool)*VP_WIDTH*VP_HEIGHT);
}

float simplify_angle(float theta) {
  static const float PI_2 = GS_PI * 2.f;

  while(theta > PI_2) {
    theta -= PI_2;
  }
  while(theta < 0.f) {
    theta += PI_2;
  }
  return theta;
}

float dist(float x0, float y0, float x1, float y1) {
  return sqrtf(powf(fabsf(x1-x0),2.f)+powf(fabsf(y1-y0),2.f));
}

bool approx_zero(float x) {
  return fabsf(x) < 0.0000002;
}

typedef struct {
  float dist;
  //0 for horizontal wall, 1 for vertical wall
  int type;
  int id;
  int slice;
} hitinfo_t;

hitinfo_t shoot_ray(float x, float y, float theta) {
  float tan_theta = tanf(theta);
  float dist_v = -1.f, dist_h = -1.f;
  float id_v, id_h;

  float dir_x = cosf(theta), dir_y = sinf(theta);
  float dx = 0.f, dy = 0.f;
  if(dir_x > 0.f) {
    dx = ceilf(x)-x;
  }
  else if(dir_x < 0.f) {
    dx = floorf(x)-x;
  }
  if(dir_y > 0.f) {
    dy = ceilf(y)-y;
  }
  else if(dir_y < 0.f) {
    dy = floorf(y)-y;
  }
  float cur_xh, cur_xv, cur_yh, cur_yv;

  //dist_h, checking for horizontally aligned walls
  if(!approx_zero(dir_y)) {
    //printf("dist_h check\n");
    //printf("%.8f\n", dir_y);
    float nx = dy/tan_theta, ny = dy;
    float step_x = 1.f/tan_theta, step_y = 1.f;
    int offset = 0;
    if(dir_y < 0.f) {
      step_x *= -1.f;
      step_y *= -1.f;
      offset = 1;
    }
    cur_xh = x+nx;
    cur_yh = y+ny;
    for(int i = 0; i < dof; i++) {
      int int_x = (int)cur_xh;
      int int_y = (int)(cur_yh)-offset;
      if(int_x >= 0 && int_x < map_width && int_y >= 0 && int_y < map_height) {
        int tile_id = level[int_y*map_width+int_x].wall;
        if(tile_id > 0) {
          dist_h = dist(x,y,cur_xh, cur_yh);
          id_h = tile_id;
          break;
        }
      }
      cur_xh += step_x;
      cur_yh += step_y;
    }
  }

  //dist_v, checking for vertically aligned walls
  if(!approx_zero(dir_x)) {
    //printf("dist_v check\n");
    //printf("%.8f\n", dir_x);
    float nx = dx, ny = dx*tan_theta;
    float step_x = 1.f, step_y = tan_theta;
    int offset = 0;
    if(dir_x < 0.f) {
      step_x *= -1.f;
      step_y *= -1.f;
      offset = 1;
    }
    cur_xv = x+nx;
    cur_yv = y+ny;
    for(int i = 0; i < dof; i++) {
      int int_x = (int)(cur_xv)-offset;
      int int_y = (int)(cur_yv);
      if(int_x >= 0 && int_x < map_width && int_y >= 0 && int_y < map_height) {
        int tile_id = level[int_y*map_width+int_x].wall;
        if(tile_id > 0) {
          dist_v = dist(x,y,cur_xv, cur_yv);
          id_v = tile_id;
          break;
        }
      }
      cur_xv += step_x;
      cur_yv += step_y;
    }
  }

  //printf("making raycast result\n");
  hitinfo_t res;
  if(dist_v <= 0.f) {
    res.dist = dist_h;
    res.type = 0;
    res.id = id_h;
  }
  else if(dist_h <= 0.f) {
    res.dist = dist_v;
    res.type = 1;
    res.id = id_v;
  }
  else if(dist_v < dist_h) {
    res.dist = dist_v;
    res.type = 1;
    res.id = id_v;
  }
  else { 
    res.dist = dist_h;
    res.type = 0;
    res.id = id_h;
  }

  if(res.type == 0) {
    res.slice = (int)(cur_xh*64.f)%64; //+1
  }
  else {
    res.slice = (int)(cur_yv*64.f)%64;
  }

  return res;
}

void draw_walls() {
  gs_asset_texture_t *tp = gs_assets_getp(&gsa, gs_asset_texture_t, wall_tex_hndl);
  for(int i = 0; i < VP_WIDTH; i++) {
    //printf("-----\nstarting strip: %d\n",i);
    float theta = simplify_angle(i * ray_angle + (player.dir - fov/2.f));
    hitinfo_t rayhit = shoot_ray(player.pos.x, player.pos.y, theta);
    //printf("ray shot: %d\n",i);
    float dist = rayhit.dist * cosf(player.dir-theta);
    int line_height = VP_HEIGHT/dist;
    //printf("%f, %d\n", dist, line_height);
    if (line_height > 0.f) {
      int c = (int)(dist/dof*255.f);
      gs_color_t color;
      if(rayhit.type == 0) {
        color = gs_color(c/2,c/2,c/2,255);
      }
      else {
        color = gs_color(c,c,c,255);
      }
      //printf("drawing strip, %f, %d\n", dist, line_height);
      //vp_draw_strip(i, line_height, color);
      zbuf[i] = dist;
      vp_draw_strip_tex(i, line_height, rayhit.slice, &tp->desc, color);
    }
    //printf("strip drawn: %d\n",i);
  }
}

void draw_floor_ceil() {
  gs_asset_texture_t *ftp = gs_assets_getp(&gsa, gs_asset_texture_t, floor_tex_hndl);
  gs_asset_texture_t *ctp = gs_assets_getp(&gsa, gs_asset_texture_t, ceil_tex_hndl);
  //printf("%f,%f\n",player.pos.x,player.pos.y);
  for(int y = VP_HEIGHT/2+0; y < VP_HEIGHT; y++) {
    float theta;
    theta = simplify_angle(player.dir - fov/2.f);
    float dir_x0 = plane_mag*cosf(theta), dir_y0 = plane_mag*sinf(theta);
    theta = simplify_angle(player.dir + fov/2.f);
    float dir_x1 = plane_mag*cosf(theta), dir_y1 = plane_mag*sinf(theta);

    int p = y - VP_HEIGHT/2; // /2
    float pos_z = 1.f * (float)VP_HEIGHT; // * 0.5f
    float row_dist = pos_z / p;

    float step_x = row_dist * (dir_x1 - dir_x0) / (float)VP_WIDTH;
    float step_y = row_dist * (dir_y1 - dir_y0) / (float)VP_WIDTH;

    float floor_x = player.pos.x + row_dist*dir_x0;
    float floor_y = player.pos.y + row_dist*dir_y0;
    //float floor_x = player.pos.x/plane_mag + row_dist*dir_x0;
    //float floor_y = player.pos.y/plane_mag + row_dist*dir_y0;
    //float floor_x = player.pos.x/1.15f + row_dist*dir_x0;
    //float floor_y = player.pos.y/1.15f + row_dist*dir_y0;
    //float floor_x = player.pos.x/?.f + row_dist*dir_x0; //approx 1.15f??
    //float floor_y = player.pos.y/?.f + row_dist*dir_y0;

    for(int x = 0; x < VP_WIDTH; x++) {
      int cell_x = (int)floor_x, cell_y = (int)floor_y;

      int tx = (int)(64 * (floor_x - cell_x)) & (64-1);
      int ty = (int)(64 * (floor_y - cell_y)) & (64-1);

      floor_x += step_x;
      floor_y += step_y;

      gs_color_t c;
      c = texture_get_point(&ftp->desc, tx,ty);
      //if (!(is_even(cell_x) && is_odd(cell_y)) && !(is_odd(cell_x) && is_even(cell_y))) c = GS_COLOR_BLUE;
      //else c = GS_COLOR_RED;
      vp_draw_pixel(x,y, c);
      c = texture_get_point(&ctp->desc, tx,ty);
      vp_draw_pixel(x, VP_HEIGHT-y-1, c);
    }
  }
}

void draw_map(float x0, float y0, float x1, float y1) {
  const int scale = 10;
  for(int y = 0; y < map_height; y++) {
    for(int x = 0; x < map_width; x++) {
      for(int i = 0; i < scale; i++) {
        for(int j = 0; j < scale; j++) {
          vp_draw_pixel(scale*x+j,scale*y+i,GS_COLOR_BLACK);
        }
      }
    }
  }
  int x = x0*(float)scale;
  int y = y0*(float)scale;
  vp_draw_pixel(x,y, GS_COLOR_WHITE);
  x = x1*(float)scale;
  y = y1*(float)scale;
  vp_draw_pixel(x,y, GS_COLOR_RED);
}

void draw_sprite(const actor_t *tf) {
  gs_asset_texture_t *tp = gs_assets_getp(&gsa, gs_asset_texture_t, barrel_tex_hndl);
  float sx = tf->pos.x - player.pos.x;
  float sy = tf->pos.y - player.pos.y;
  float theta = -simplify_angle(player.dir);
  float cost = cosf(theta);
  float sint = sinf(theta);

  //float dirx = cost;
  //float diry = sint;
  //float planex = 
  //float inv_del = 1.0f / ();
  float tsy = sx*cost - sy*sint;
  float tsx = sy*cost + sx*sint;
  //tsx*=plane_mag;
  //tsy*=plane_mag;
  //printf("---------\n");
  //printf("%f, %f\n", player.pos.x, player.pos.y);
  //printf("%f: %f, %f\n", theta, tsx, tsy);

  int screen_x = (int)((VP_WIDTH/2.f) * (1.f + tsx/tsy));
  //printf("%d\n", screen_x);

  int sprite_height = abs((int)(VP_HEIGHT/tsy));
  int draw_start_y = -sprite_height/2 + VP_HEIGHT/2;
  if(draw_start_y < 0) draw_start_y = 0;
  int draw_end_y = sprite_height/2 + VP_HEIGHT/2;
  if(draw_end_y >= VP_HEIGHT) draw_end_y = VP_HEIGHT-1;

  int sprite_width = abs((int)(VP_HEIGHT/tsy));
  int draw_start_x = -sprite_width/2 + screen_x;
  if(draw_start_x < 0) draw_start_x = 0;
  int draw_end_x = sprite_width/2 + screen_x;
  if(draw_end_x >= VP_WIDTH) draw_end_x = VP_WIDTH;

  //printf("%d, %d, %d\n", sprite_width, draw_start_x, draw_end_x);
  for(int stripe = draw_start_x; stripe < draw_end_x; stripe++) {
    if(tsy > 0 && stripe >= 0 && stripe < VP_WIDTH && tsy < zbuf[stripe]) {
      int rstartx = -sprite_width/2 + screen_x, rstarty = -sprite_height/2 + VP_HEIGHT/2;
      int rendx = sprite_width/2 + screen_x, rendy = sprite_height/2 + VP_HEIGHT/2;
      int tex_x = (int)((float)(stripe-rstartx)/(float)(rendx-rstartx)*64.f);
      for(int y = draw_start_y; y < draw_end_y; y++) {
        int tex_y = (int)((float)(y-rstarty)/(float)(rendy-rstarty)*64.f);
        vp_draw_pixel(stripe,y, texture_get_point(&tp->desc, tex_x, tex_y));
      }
    }
  }
  //draw_map(player.pos.x, player.pos.y, tf->pos.x, tf->pos.y);
  //draw_map(player.pos.x, player.pos.y, tsx+player.pos.x, tsy+player.pos.y);
}

// coord to viewport index
int c2i(int x, int y) {
  return y*VP_WIDTH+x;
}

void vp_draw_pixel(int x, int y, gs_color_t c) {
  if(x < 0 || x >= VP_WIDTH || y < 0 || y >= VP_HEIGHT || c.a < 255) {
    return;
  }
  int ix = c2i(x,y);
  if (stencil_enabled ? stencil[ix] : true) {
    viewport[ix] = c;
    stencil[ix] = false;
  }
}

void vp_draw_strip(int x, int h, gs_color_t c) {
  for(int i = VP_HEIGHT/2 - h; i <= VP_HEIGHT/2+h; i++) {
    //viewport[c2i(x,i)] = c;
    vp_draw_pixel(x,i,c);
  }
}

gs_color_t texture_get_point(gs_graphics_texture_desc_t *td, int x, int y) {
  gs_color_t *pixels = td->data;
  int theight = td->height;
  int twidth = td->width;
  if (pixels == NULL || x < 0 || x >= twidth || y < 0 || y >= theight){
    return GS_COLOR_BLACK;
  }

  return pixels[y*twidth+x];
}

void vp_draw_tex(int vx, int vy, int tx0, int ty0, int tx1, int ty1, gs_graphics_texture_desc_t *td) {
  gs_color_t *pixels = td->data;
  if (pixels == NULL || pixels){
    printf("vp_draw_strip_tex: pixel data null\n");
    return;
  }
  int theight = td->height;
  int twidth = td->width;

  gs_color_t c;
  for(int y = ty0; y < ty1; y++) {
    for(int x = tx0; x < tx1; x++) {
      c = pixels[y*twidth+x];
      vp_draw_pixel(vx+x, vy+y, c);
    }
  }
}

void vp_draw_strip_tex(int x, int h, int s, gs_graphics_texture_desc_t *td, gs_color_t color_mod) {
  if(s < 0 || s >= 64) {
    return;
  }

  gs_color_t *pixels = td->data;
  if (pixels == NULL){
    printf("vp_draw_strip_tex: pixel data null\n");
    return;
  }
  int theight = td->height;
  int twidth = td->width;

  gs_color_t c;
  int count = 0;
  for(int i = VP_HEIGHT/2 - h + 1; i <= VP_HEIGHT/2+h; i++) {
    int p = (int)(((float)count)/((float)h*2.f)*64.f);
    c = pixels[p * twidth + s];
    c.r = gs_max(0, c.r-color_mod.r);
    c.g = gs_max(0, c.g-color_mod.g);
    c.b = gs_max(0, c.b-color_mod.b);
    vp_draw_pixel(x,i,c);
    count++;
  }
}

void vp_clear(gs_color_t c) {
  for(int i = 0; i < VP_WIDTH*VP_HEIGHT; i++) {
    viewport[i] = c;
  }
}

void app_input() {
  // Quit engine/app
  if (gs_platform_key_pressed(GS_KEYCODE_ESC)) gs_quit();


  const float rot = 1.5f;
  if (gs_platform_key_down(GS_KEYCODE_LEFT)) {
    player.dir -= dt * rot;
  }
  if (gs_platform_key_down(GS_KEYCODE_RIGHT)) {
    player.dir += dt * rot;
  }
  const float speed = 2.f;
  if (gs_platform_key_down(GS_KEYCODE_W)) {
    float dx = dt * speed * cosf(player.dir);
    float dy = dt * speed * sinf(player.dir);
    player.pos.x += dx;
    player.pos.y += dy;
  }
  if (gs_platform_key_down(GS_KEYCODE_S)) {
    float dx = dt * (-speed) * cosf(player.dir);
    float dy = dt * (-speed) * sinf(player.dir);
    player.pos.x += dx;
    player.pos.y += dy;
  }
  if (gs_platform_key_down(GS_KEYCODE_A)) {
    float dx = dt * (-speed) * cosf(player.dir+GS_PI/2.f);
    float dy = dt * (-speed) * sinf(player.dir+GS_PI/2.f);
    player.pos.x += dx;
    player.pos.y += dy;
  }
  if (gs_platform_key_down(GS_KEYCODE_D)) {
    float dx = dt * speed * cosf(player.dir+GS_PI/2.f);
    float dy = dt * speed * sinf(player.dir+GS_PI/2.f);
    player.pos.x += dx;
    player.pos.y += dy;
  }
}

void app_init() {
  //plane_mag = sinf(fov/2.f)/sinf(GS_PI/2.f - fov/2.f);
  plane_mag = 1.f/sinf(GS_PI/2.f - fov/2.f);
  printf("%.4f, %.4f\n",plane_mag, 1.f/plane_mag);
  //engine stuff
  gcb = gs_command_buffer_new();
  gsa = gs_asset_manager_new();
  gsi = gs_immediate_draw_new(gs_platform_main_window());

  gs_color_t *pixels = viewport;
  vp_tex.desc = (gs_graphics_texture_desc_t) {
    .width = VP_WIDTH,
    .height = VP_HEIGHT,
    .format = GS_GRAPHICS_TEXTURE_FORMAT_RGBA8,
    .min_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST, 
    .mag_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST,
    .data = pixels
  };
  vp_tex.hndl = gs_graphics_texture_create(&vp_tex.desc);
  printf("%p, %p\n", vp_tex.desc.data, pixels);

  gs_asset_texture_t wall_tex = {0};
  gs_asset_texture_load_from_file("./assets/wall.png", &wall_tex, NULL, false, true);
  wall_tex_hndl = gs_assets_create_asset(&gsa, gs_asset_texture_t, &wall_tex);

  gs_asset_texture_t floor_tex = {0};
  gs_asset_texture_load_from_file("./assets/floor.png", &floor_tex, NULL, false, true);
  floor_tex_hndl = gs_assets_create_asset(&gsa, gs_asset_texture_t, &floor_tex);
 
  gs_asset_texture_t ceil_tex = {0};
  gs_asset_texture_load_from_file("./assets/ceiling.png", &ceil_tex, NULL, false, true);
  ceil_tex_hndl = gs_assets_create_asset(&gsa, gs_asset_texture_t, &ceil_tex);

  gs_asset_texture_t barrel_tex = {0};
  gs_asset_texture_load_from_file("./assets/champ.png", &barrel_tex, NULL, false, true);
  barrel_tex_hndl = gs_assets_create_asset(&gsa, gs_asset_texture_t, &barrel_tex);

  gs_asset_font_t fnt = {0};
  gs_asset_font_load_from_file("./assets/font.ttf", &fnt, 32);
  fnt_handle = gs_assets_create_asset(&gsa, gs_asset_font_t, &fnt);

  //game stuff
  player = actor_new(2.f, 2.f, 0);
  barrel.tf.pos = gs_v2(5.f,2.f);
}

void app_draw() {
  const gs_vec2 fb = gs_platform_framebuffer_sizev(gs_platform_main_window());
  const gs_vec2 ws = gs_platform_window_sizev(gs_platform_main_window());

  int scale_x = (int)(ws.x / VP_WIDTH);
  int scale_y = (int)(ws.y / VP_HEIGHT);
  int scale = scale_x < scale_y ? scale_x : scale_y;
  if(scale < 1) {
    scale = 1;
  }

  int scaled_width = VP_WIDTH * scale;
  int scaled_height = VP_HEIGHT * scale;

  gs_asset_font_t *fp = gs_assets_getp(&gsa, gs_asset_font_t, fnt_handle);

  stencil_enabled = true;
  //vp_clear(gs_color(40,40,40,255));
  stencil_clear();
  zbuf_clear();
  draw_walls();
  draw_floor_ceil();
  stencil_enabled = false;
  draw_sprite(&barrel.tf);

  gs_graphics_texture_request_update(&gcb, vp_tex.hndl, &vp_tex.desc);
  gsi_camera2D(&gsi);
  gsi_translatef(&gsi, (ws.x-scaled_width)/2, (ws.y-scaled_height)/2, 0.f);
  gsi_texture(&gsi, vp_tex.hndl);
  gsi_rect(&gsi, 0.f, 0.f, scaled_width, scaled_height, 255, 255, 255, 255, GS_GRAPHICS_PRIMITIVE_TRIANGLES);

  gsi_defaults(&gsi);
  //gsi_camera2D(&gsi);
  char str[6] = {0};
  gs_snprintf(str, 6, "%.2f", fps);
  gsi_text(&gsi, 10.f, 10.f, str, fp, false, 255, 255, 255, 255);


  //gs_graphics_clear_desc_t clear = {.actions = &(gs_graphics_clear_action_t){.color = 0.1f, 0.1f, 0.1f, 255}};
  // Bind render pass for backbuffer
  gs_graphics_begin_render_pass(&gcb, GS_GRAPHICS_RENDER_PASS_DEFAULT);
  gs_graphics_set_viewport(&gcb, 0, 0, (int32_t)fb.x, (int32_t)fb.y);
  //gs_graphics_clear(&gcb, &clear);
  gsi_draw(&gsi, &gcb); // Binds pipelines and submits to graphics command buffer for rendering

  gs_graphics_end_render_pass(&gcb);
}

void app_update() {
  // Increment time and fps
  dt = gs_platform_delta_time();
  et = gs_platform_elapsed_time();
  fps_timer += dt;
  if(fps_timer >= 1.f) {
    fps_timer = 0.f;
    fps = 1.f/dt;
  }

  // Update inputs
  app_input();

  // Render
  app_draw();

  // Submit command buffer (syncs to GPU, MUST be done on main thread where you have your GPU context created)
  gs_graphics_submit_command_buffer(&gcb);
}

gs_app_desc_t gs_main(int32_t argc, char** argv) {
  return (gs_app_desc_t) {
    .init = app_init,
    .update = app_update
  };
}


